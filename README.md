# colorlight-breakout-io-buffer

Add level-shifting buffered IO between colorlight breakout board and target devices

## Renders

![Render of front of PCB](/renders/front.png "Front of PCB")

![Render of back of PCB](/renders/back.png "Back of PCB")
